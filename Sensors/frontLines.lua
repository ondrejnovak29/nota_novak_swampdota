local sensorInfo = {
	name = "frontLines",
	desc = "Return parsed corridors with locations of front lines.",
	author = "Ondřej Novák",
	date = "2020-07-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- speedups
local SpringGetUnitAllyTeam = Spring.GetUnitAllyTeam
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere


-- @description horizontal euclidean distance between 2 points
function euclDist(x1, z1, x2, z2)
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
end

-- @description return whether the position is occupied by allied team
function isAllied(pos)
    local units = SpringGetUnitsInSphere(pos.x, pos.y, pos.z, 200)

    for _, uid in ipairs(units) do
        if SpringGetUnitAllyTeam(uid) == 0 then
            return true
        end
    end

    return false
end

-- @description return last allied point
function frontLine(points)
    for _, point in ipairs(points) do
        point.friendly = true
    end

	local lastAllied = nil
    for i=#points, 1, -1 do
        if isAllied(points[i].position) then
			lastAllied = points[i].position
            break
        else
            points[i].friendly = false
        end
	end

    return lastAllied
end

-- @description returns series of points with added interpolated points between each neighbor
function interpolatePoints(points, numPoints)
    local newPoints = {points[1]}

    for i=2, #points do
        
        local dx = points[i].position.x - points[i - 1].position.x
        local dy = points[i].position.y - points[i - 1].position.y
        local dz = points[i].position.z - points[i - 1].position.z

        for j=1, numPoints do
            table.insert(newPoints, {
                isStrongpoint = false,
                position={
                    x=points[i - 1].position.x + dx * j / (numPoints + 1),
                    y=points[i - 1].position.y + dy * j / (numPoints + 1),
                    z=points[i - 1].position.z + dz * j / (numPoints + 1)
                }
            })
        end

        table.insert(newPoints, points[i])
    end

    return newPoints
end

-- @description return parsed corridor
function parseLane(points)
    points = interpolatePoints(points, 5)

    local fl = frontLine(points)

    local distances = {}
    for _, point in ipairs(points) do
        point.distance = euclDist(fl.x, fl.z, point.position.x, point.position.z)
    end

    return points
end


-- @description return parsed corridors with locations of front lines
return function()
    local cor = global.info.corridors

    global.frontLines = {
		top=parseLane(cor.Top.points),
		mid=parseLane(cor.Middle.points),
		bot=parseLane(cor.Bottom.points) 
	}
end
