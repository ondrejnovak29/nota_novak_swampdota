local sensorInfo = {
	name = "unitToBuy",
	desc = "Return which unit should be bought next.",
	author = "Ondřej Novák",
	date = "2020-07-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- speedups
local SpringGetUnitIsDead = Spring.GetUnitIsDead
local SpringGetUnitPosition = Spring.GetUnitPosition


-- units' defIds and their desired number
UNITS = {
    [34]={name='armbox', minNum=2}, -- box of death, immobile artilery
    [98]={name='armmart', minNum=0}, -- luger, mobile artilery
    [99]={name='armmav', minNum=3}, -- maverick, fighting ground unit
    [166]={name='armzeus', minNum=0}, -- zeus, fighting ground unit
  
    [61]={name='armfark', minNum=2}, -- FARCK, recovers metal

    [27]={name='armatlas', minNum=1}, -- atlas, flying transport
    [202]={name='cmercdrag', minNum=0}, -- dragonfly, fighting flying transport

    [115]={name='armpeep', minNum=0}, -- peeper, flying scout
    [144]={name='armspy', minNum=0}, -- infiltrator, ground scout
    [132]={name='armseer', minNum=1} -- seer, mobile radar
}

-- which order should units be bought in
PRIORITY = {
    132,
    27,
    34,
    99,
    61
}


-- @description select for which lane the unit should be bought
function selectLane()
    return 'mid'
end

-- @description returns units with give defId in given lane
function unitsOfType(lane, defId)
    if lane[defId] == nil then
        return {}
    end

    local notDead = {}
    for _, uid in ipairs(lane[defId]) do
        local dead = SpringGetUnitIsDead(uid)

        local _, _, z = SpringGetUnitPosition(uid)

        if not (dead or dead == nil) and z ~= nil and z > 250 then
            table.insert(notDead, uid)
        end
    end

    return notDead
end


-- @description return which unit should be bought next
return function()
    local lane = selectLane()

    for _, defId in ipairs(PRIORITY) do
        global.units[lane][defId] = unitsOfType(global.units[lane], defId)

        -- if we have less than the desired number of a given unit, buy more
        if #global.units[lane][defId] < UNITS[defId].minNum then
            return {lane=lane, defId=defId, name=UNITS[defId].name}
        end
    end

    -- as a default, buy mavericks
    return {lane=lane, defId=99, name=UNITS[99].name}
end
