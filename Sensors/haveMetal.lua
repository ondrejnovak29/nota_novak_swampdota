local sensorInfo = {
	name = "haveMetal",
	desc = "return whether team has enough to purchase given unit.",
	author = "Ondřej Novák",
	date = "2020-07-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- speedups
local SpringGetTeamResources = Spring.GetTeamResources

local myTeamId = Spring.GetMyTeamID()


-- @description return whether team has enough to purchase given unit
return function(defId)
	return SpringGetTeamResources(myTeamId, "metal") > global.info.buy[defId]
end
