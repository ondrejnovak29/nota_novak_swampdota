local sensorInfo = {
	name = "initializeUnits",
	desc = "Return assignment of units the player starts with.",
	author = "Ondřej Novák",
	date = "2020-07-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- speedups
local SpringGetTeamUnitsByDefs = Spring.GetTeamUnitsByDefs

local myTeamId = Spring.GetMyTeamID()


-- @description return assignment of units the player starts with
return function()
    local mid, assigned = {}, {} 

    -- add atlases and boxes of death to the middle lane
    mid[27] = SpringGetTeamUnitsByDefs(myTeamId, 27)
    mid[34] = SpringGetTeamUnitsByDefs(myTeamId, 34)

    -- add units to the 'assigned' table
    for _, defId in ipairs({27, 34}) do
        for _, uid in ipairs(mid[defId]) do
            table.insert(assigned, uid)
        end 
    end

    return {
        top={}, 
        mid=mid,
        bot={},
        assigned=assigned
    }
end
