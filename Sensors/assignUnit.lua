local sensorInfo = {
	name = "assignUnit",
	desc = "Assign unit with given defId to a lane.",
	author = "Ondřej Novák",
    date = "2020-07-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- speedups
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere


-- @description return table's values in a new table as keys
function valueSet(table)
    local t = {}
    for _, v in ipairs(table) do
        t[v] = true
    end
    return t
end


-- @description assign unit with given defId to a lane
return function(lane, defId)
    local baseUnits = SpringGetUnitsInSphere(global.base.x, global.base.y, global.base.z, 3000)
    local assigned = valueSet(global.units.assigned)

    -- check each unit in base
    local unitId = nil
    for _, uid in ipairs(baseUnits) do
        if SpringGetUnitDefID(uid) == defId and not assigned[uid] then
            unitId = uid
            break
        end
    end

    -- initialize given unit's lane table if necessary
    if global.units[lane][defId] == nil then
        global.units[lane][defId] = {}
    end
    
    table.insert(global.units[lane][defId], unitId)
    table.insert(global.units.assigned, unitId)
end
