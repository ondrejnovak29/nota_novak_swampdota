local sensorInfo = {
	name = "getBasePosition",
	desc = "Return positions of the base.",
	author = "Ondřej Novák",
	date = "2020-07-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetTeamUnitsByDefs = Spring.GetTeamUnitsByDefs

local myTeamId = Spring.GetMyTeamID()


-- @description return positions of the base
return function()
    local base = SpringGetTeamUnitsByDefs(myTeamId, 112)[1]
    local x, y, z = SpringGetUnitPosition(base)

    return {x=x, y=y, z=z}
end
