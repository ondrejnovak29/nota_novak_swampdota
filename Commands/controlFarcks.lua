function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Controls movement of FARCKs in given lane.",
		parameterDefs = {
			{ 
				name = "where",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
		}
	}
end


-- speedups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit


function Run(self, units, p)
    local points = global.frontLines[p.where]
    
    -- get nearest point to front line that is at least 1000 units away
    local destPoint = points[1]
    for i=#points, 1, -1 do
        if points[i].friendly and points[i].distance > 1100 and points[i].distance < destPoint.distance then
            destPoint = points[i]
            break
        end
    end

    -- give orders to all Mavericks
    local dest = destPoint.position
    for _, uid in ipairs(global.units[p.where][61] or {}) do
        SpringGiveOrderToUnit(
            uid,
            CMD.RECLAIM,
            {dest.x, dest.y, dest.z, 700},
            {}
        )
    end

    return SUCCESS
end

function Reset(self)
    return self
end
