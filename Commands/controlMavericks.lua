function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Controls movement of Mavericks in given lane.",
		parameterDefs = {
			{ 
				name = "where",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
		}
	}
end


-- speedups
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere


-- @description count the number of Mavericks around given position
function countMavericks(position)
    local uids = SpringGetUnitsInSphere(position.x, position.y, position.z, 1200)
    local count = 0
    for _, uid in ipairs(uids) do
        if SpringGetUnitDefID(uid) == 99 then
            count = count + 1
        end
    end
    return count
end


local function ClearState(self)
    self.attacking = nil
end

function Run(self, units, p)
    local points = global.frontLines[p.where]

    -- get nearest point to front line that is at least 200 units away 
    local destPoint, strongpoint, spI = points[1], points[1], 1
    for i=#points, 1, -1 do
        if not points[i].friendly and points[i].isStrongpoint then
            spI = i
            strongpoint = points[i]
        end

        if destPoint.position.x == points[1].position.x and points[i].friendly and points[i].distance > 400 and points[i].distance < destPoint.distance then
            destPoint = points[i]
        end
    end

    local mavCount = countMavericks(strongpoint.position)
    if self.attacking ~= nil then
        if mavCount == 0 or points[self.attacking].friendly then
            self.attacking = nil
        end
    else
        -- give orders to all Mavericks
        local dest = destPoint.position

        if mavCount >= 7 then
            self.attacking = spI
            dest = strongpoint.position 
        end

        if spI == 1 then
            dest = points[#points].position
        end

        for _, uid in ipairs(global.units[p.where][99] or {}) do
            SpringGiveOrderToUnit(
                uid,
                CMD.MOVE,
                {dest.x + math.random(-50, 50), dest.y, dest.z + math.random(-50, 50)},
                {}
            )
        end
    end

    return SUCCESS
end

function Reset(self)
    ClearState(self)
end
