function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Controls movement of Boxes of Death in given lane.",
		parameterDefs = {
			{ 
				name = "where",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
		}
	}
end


-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitVelocity = Spring.GetUnitVelocity
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting


-- how far from the strongpoint the box should land
DISTANCE_TO_LAND = 830


-- @description horizontal euclidean distance between 2 points
function euclDist(x1, z1, x2, z2)
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
end

-- @description return id of a Box of Death that is out of place
function chooseOutOfPlace(bods, strongpoint)
    if strongpoint == nil then
        return nil
    end

    for _, bod in ipairs(bods) do
        local x, _, z = SpringGetUnitPosition(bod)
        if x ~= nil and z ~= nil and euclDist(strongpoint.position.x, strongpoint.position.z, x, z) > 950 then
            return bod
        end
    end

    return nil
end

-- @description finds position in a safe distance away from a given strongpoint
function locationToLand(prevpoint, strongpoint)
    -- angle pointing from strongpoint to allied position
    local safeDir = math.atan2(-prevpoint.z + strongpoint.z, prevpoint.x - strongpoint.x)

    -- angual offset from perpendicular direction
    local offset = 0
    while offset < math.pi / 2 do
        -- destination relative to strongpoint
        local rand = math.random(-2, 2)

        local dirX  = DISTANCE_TO_LAND * math.cos(safeDir - math.pi / 2 + offset) + rand * 25
        local dirZf = DISTANCE_TO_LAND * math.sin(safeDir - math.pi / 2 + offset) + rand * 25

        -- absolute destination
        local destX, destZ = strongpoint.x + dirX, strongpoint.z - dirZf
        local destY = SpringGetGroundHeight(destX, destZ)
        
        -- ensure destination isn't water or high ground
        if destY > 30 and destY < 100 then
            return {
                x=destX,
                y=destY,
                z=destZ
            }
        end

        -- move around circle
        offset = offset + 0.05
    end
end

-- @description return points in given corridor and index of first enemy strongpoint
function getPoints(p)
    -- points in given corridor
    local points = global.frontLines[p.where]

    -- index of next uncaptured strongpoint
    local strongIdx = 2
    for i=3, #points do
        if points[i].isStrongpoint and points[i].ownerAllyID == 1 then
            strongIdx = i
            break
        end
    end

    return points, strongIdx
end

-- @description return move parameters, nil if move cannot or should not be performed
function getMoveParameters(p, points, strongIdx)
    local target = locationToLand(points[strongIdx - 1].position, points[strongIdx].position)

    local tran = (global.units[p.where][27] or {})[1]

    local bod = chooseOutOfPlace(global.units[p.where][34] or {}, points[strongIdx])

    return target, tran, bod
end


local function ClearState(self)
    self.status = nil
end

function Run(self, units, p)
    local points, strongIdx = getPoints(p)

    local target, tran, bod = getMoveParameters(p, points, strongIdx)

    -- check if move should be performed
    if tran == nil or bod == nil or target == nil then
        self.status = nil
        return SUCCESS
    end

    -- if transport in this lane is idle
    if self.status == nil then
        SpringGiveOrderToUnit(
            tran,
            CMD.LOAD_UNITS,
            {bod},
            {}
        )

        SpringGiveOrderToUnit(
            tran,
            CMD.UNLOAD_UNIT,
            {target.x, target.y, target.z},
            {"shift"}
        )

        -- point located in allied half of the map
        local safePoint = points[math.floor(#points / 3)]
        SpringGiveOrderToUnit(
            tran,
            CMD.MOVE,
            {safePoint.position.x, safePoint.position.y, safePoint.position.z},
            {"shift"}
        )

        self.status = 'load'
    end

    -- check if transport started carrying unit
    local carrying = SpringGetUnitIsTransporting(tran)
    if carrying ~= nil and #carrying > 0 then
        self.status = 'carry'
    end

    -- check if transport is stationary
    local _, _, _, vel = SpringGetUnitVelocity(tran)
    if vel == nil or vel < 0.1 then
        self.status = nil
    end

    -- check if transport unloaded unit
    if self.status == 'carry' and carrying ~= nil and #carrying == 0 then
        self.status = nil
        return SUCCESS
    else 
        return RUNNING
    end
end

function Reset(self)
    ClearState(self)
end
