function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Controls movement of Seers in given lane.",
		parameterDefs = {
			{ 
				name = "where",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
		}
	}
end


-- speedups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit


function Run(self, units, p)
    local points = global.frontLines[p.where]

    -- get nearest point to front line that is at least 1750 units away 
    local destPoint = points[1]
    for i=#points, 1, -1 do
        if points[i].friendly and points[i].distance > 1700 and points[i].distance < destPoint.distance then
            destPoint = points[i]
            break
        end
    end

    -- give orders to all Seers
    local dest = destPoint.position
    for _, uid in ipairs(global.units[p.where][132] or {}) do
        SpringGiveOrderToUnit(
            uid,
            CMD.MOVE,
            {dest.x + math.random(-50, 50), dest.y, dest.z + math.random(-50, 50)},
            {}
        )
    end

    return SUCCESS
end

function Reset(self)
    return self
end
